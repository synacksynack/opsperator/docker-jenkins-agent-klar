image: registry.gitlab.com/gdunstone/docker-buildx-qemu

services:
  - name: docker:dind
    entrypoint: ["env", "-u", "DOCKER_HOST"]
    command: ["dockerd-entrypoint.sh"]

stages:
  - build
  - test
  - release

variables:
  CI_BUILD_ARCHS: linux/arm/v7,linux/arm64/v8,linux/amd64
  DOCKER_DRIVER: overlay2
  DOCKER_HOST: tcp://docker:2375/
  DOCKER_TLS_CERTDIR: ""
  IMAGE_ADDR: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_SLUG
  IMAGE_CACHE: $CI_REGISTRY_IMAGE/cache:$CI_COMMIT_SHA
  IMAGE_FINAL: $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME
  OLD_IMAGE_ADDR: $CI_REGISTRY_IMAGE:el7-$CI_COMMIT_REF_SLUG
  OLD_IMAGE_CACHE: $CI_REGISTRY_IMAGE/cache:el7-$CI_COMMIT_SHA
  OLD_IMAGE_FINAL: $CI_REGISTRY_IMAGE:el7-$CI_COMMIT_REF_NAME
  TKN_IMAGE_ADDR: $CI_REGISTRY_IMAGE:tkn-$CI_COMMIT_REF_SLUG
  TKN_IMAGE_CACHE: $CI_REGISTRY_IMAGE/cache:tkn-$CI_COMMIT_SHA
  TKN_IMAGE_FINAL: $CI_REGISTRY_IMAGE:tkn-$CI_COMMIT_REF_NAME

build-tkn:
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
    - update-binfmts --enable
    - docker buildx create --driver docker-container --use
  script:
    - >
        docker buildx build --platform $CI_BUILD_ARCHS \
            --cache-from=type=registry,ref=$TKN_IMAGE_CACHE \
            --cache-to=type=registry,ref=$TKN_IMAGE_CACHE \
            --progress plain --pull --push \
            -f Dockerfile.debian -t "$TKN_IMAGE_ADDR" .

build8:
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $IMAGE_ADDR -f Dockerfile.el8 .
    - docker push $IMAGE_ADDR

build7:
  stage: build
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - docker build -t $OLD_IMAGE_ADDR -f Dockerfile.el7 .
    - docker push $OLD_IMAGE_ADDR

scan-tkn:
  stage: test
  image:
    name: docker.io/aquasec/trivy
    entrypoint: [""]
  script:
    - trivy image "$TKN_IMAGE_ADDR" || echo too-bad

scan8:
  stage: test
  image:
    name: docker.io/aquasec/trivy
    entrypoint: [""]
  script:
    - trivy image "$IMAGE_ADDR" || echo too-bad

scan7:
  stage: test
  image:
    name: docker.io/aquasec/trivy
    entrypoint: [""]
  script:
    - trivy image "$OLD_IMAGE_ADDR" || echo too-bad

retag:
  stage: release
  image:
    name: quay.io/skopeo/stable
    entrypoint: [""]
  script:
    - >
        if test "$CI_COMMIT_TAG"; then
            skopeo copy --all \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$IMAGE_ADDR docker://$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG
        else
            skopeo copy \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$IMAGE_ADDR docker://$IMAGE_FINAL
        fi
    - >
        if test "$CI_COMMIT_TAG"; then
            skopeo copy --all \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$OLD_IMAGE_ADDR docker://$CI_REGISTRY_IMAGE:el7-$CI_COMMIT_TAG
        else
            skopeo copy \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$OLD_IMAGE_ADDR docker://$OLD_IMAGE_FINAL
        fi
    - >
        if test "$CI_COMMIT_TAG"; then
            skopeo copy --all \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$TKN_IMAGE_ADDR docker://$CI_REGISTRY_IMAGE:tkn-$CI_COMMIT_TAG
        else
            skopeo copy \
                --dest-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                --src-creds=$CI_REGISTRY_USER:$CI_REGISTRY_PASSWORD \
                docker://$TKN_IMAGE_ADDR docker://$TKN_IMAGE_FINAL
        fi

release-code:
  rules:
    - if: $CI_COMMIT_TAG
  stage: release
  image: registry.gitlab.com/gitlab-org/release-cli:latest
  release:
    name: 'Release $CI_COMMIT_TAG'
    description: '$CI_REGISTRY_IMAGE:$CI_COMMIT_TAG'
    ref: '$CI_COMMIT_TAG'
    tag_name: '$CI_COMMIT_TAG'
  script:
    - echo 'Releasing $CI_REGISTRY_IMAGE:$CI_COMMIT_TAG'
