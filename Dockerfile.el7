FROM golang:1.14.4-alpine AS builder
WORKDIR /go/src/github.com/Portshift/klar/
COPY ./config/klar/ ./
RUN CGO_ENABLED=0 go build -o klar .

FROM docker.io/openshift/jenkins-slave-base-centos7:v3.11

LABEL com.redhat.component="jenkins-agent-klar-centos7-container" \
      io.k8s.description="The jenkins agent klar image has the tools scanning Docker images against CoreOS Clair." \
      io.k8s.display-name="Jenkins Agent - Klar" \
      io.openshift.tags="openshift,jenkins,agent,clair,klar" \
      architecture="x86_64" \
      name="ci/jenkins-agent-klar" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-jenkins-agent-klar" \
      version="1.0.1"

COPY --from=builder /go/src/github.com/Portshift/klar/klar /usr/local/bin/klar

RUN if yum -y install epel-release; then \
	if test "$DO_UPGRADE"; then \
	    yum -y upgrade; \
	fi \
	&& yum -y install ca-certificates jq; \
    else \
	curl https://github.com/stedolan/jq/releases/download/jq-1.6/jq-linux64 -o /usr/bin/jq \
	&& chmod +x /usr/bin/jq; \
    fi

USER 1001
