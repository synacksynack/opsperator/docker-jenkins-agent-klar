# Klar Jenkins Agent

CentOS based Jenkins Agent shipping CoreOS Clair images scanning tools

Formerly hosted at https://github.com/faust64/docker-jenkins-agent-klar

Now using Portshift klar: https://github.com/Portshift/klar

Scan:

```
CLAIR_ADDR=http://clair-demo.demo-ci.svc:6060 \
	CLAIR_TIMEOUT=2 DOCKER_TIMEOUT=2 \
	DOCKER_USER=default \
	DOCKER_PASSWORD=$(cat /path/to/serviceaccount/token) \
	DOCKER_INSECURE=true \
	REGISTRY_INSECURE=false \
    klar docker-registry.default.svc:5000/demo-ci/jenkins-agent-klar:latest
```
